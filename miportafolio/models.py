from django.db import models

# Create your models here.


class Project(models.Model):
    titulo = models.CharField(max_length=30)
    descripcion = models.TextField(verbose_name="Descripción")
    # creando un directorio imagenes con upload_to="imagenes"
    image = models.ImageField(verbose_name="Imagen", upload_to="imagenes")
    enlaces = models.URLField(
        verbose_name="Direccion Web", null=True, blank=True)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de actulización")


def __str__(self):
    return self.titulo


class Meta:
    verbose_name = "proyecto"
    verbose_name_plural = "proyectos"
    ordering = ["-created"]
