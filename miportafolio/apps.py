from django.apps import AppConfig


class MiportafolioConfig(AppConfig):
    # Cambia la configuracion del titulo agregar  settings.py > INSTALLED_APPS asi  miportafolio.apps.MiportafolioConfig
    name = 'miportafolio'
    verbose_name = 'El portafolio'
