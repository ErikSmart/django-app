from django.contrib import admin
from .models import Project

# Register your models here.


class CambiodeNombre(admin.ModelAdmin):
    # Agregando campos a la vista que estan en models.py con verbose_name="Fecha de actulización"
    readonly_fields = ('created', 'updated')


admin.site.register(Project, CambiodeNombre)
