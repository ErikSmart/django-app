from django.shortcuts import render
from .models import Project

# Create your views here.


def portafolio(request):
    # Esta es la forma de llamar a los objetos de la base datos en django
    projectos = Project.objects.all()
    return render(request, "pagina/portafolio.html", {'projectos': projectos})
