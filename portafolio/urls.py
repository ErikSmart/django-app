"""portafolio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from core import views as core_view
from miportafolio import views as miportafolio_view
# Ver imagenes en modo Debug a parte hay que agregar en settings.py MEDIA_URL = '/media/' y MEDIA_ROOT = os.path.join(BASE_DIR, "media")
from django.conf import settings

urlpatterns = [
    path('', core_view.home, name="inicio"),
    path('contacto', core_view.contacto, name="contacto"),
    path('base', core_view.base, name="base"),
    path('acerca', core_view.acerca, name="acerca"),
    path('navegacion', core_view.navegacion, name="navegacion"),
    path('portafolio', miportafolio_view.portafolio, name="portafolio"),
    path('admin/', admin.site.urls),
]
# Ver imagenes en modo Debug
if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
