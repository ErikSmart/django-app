from django.shortcuts import render, HttpResponse

# Create your views here.

menu = """ 
<a href="/">Inicio</a>
<a href="/portafolio">Portafolio</a>
<a href="/acerca">Acerca</a>
<a href="/contacto">Contacto</a>
"""


def home(request):
    return render(request, "core/inicio.html")


def base(request):
    return render(request, base)


def contacto(request):
    return render(request, ("core/contacto.html"))


def acerca(request):
    return render(request, "core/acerca.html")


def navegacion(request):
    return render(request, "core/menu.html")


""" def acerca(request):
    return HttpResponse(menu+"<h2>Acerca</h2>") """
